const express = require('express')
const app = express()

app.get('/', function (req, res) {
  const SERVICE_NAME = process.env.SERVICE_NAME
  res.send(`Hello World from ${SERVICE_NAME}!`)
})

module.exports = app
