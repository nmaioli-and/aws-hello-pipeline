const express = require('express')
const app = express()

app.get('/', function (req, res) {
  const now = new Date()
  res.send(`It is ${now.toString()}.`)
})

module.exports = app
