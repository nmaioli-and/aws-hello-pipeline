const client = require('supertest')
  const app = require('../app')

describe('Test root path', () => {
  test('Status 200', async done => {
    client(app)
      .get('/')
      .then(res => {
        expect(res.statusCode).toBe(200)
        done()
      })
  })
})
