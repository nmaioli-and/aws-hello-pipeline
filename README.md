# Hello Pipeline

This is a toy application to work on the creation of a CI/CD pipeline
provisioned by CloudFormation with the following steps:

- Triggered via CodeCommit (push to master)
- Steps:
    + Source from CodeCommit
    + Deploy all services (helloSvc, timeSvc) from the same `buildspec.yml`
        file, leveraging environment variables, via CodeBuild

## How to

Clone this repo, create a repository on CodeCommit and push the code. You can
specify the repository name with the `RepositoryName` parameter in
CloudFormation, which defaults to `helloPipeline`. You can also specify a
branch with `BranchName`, which defaults to `master`.

Deploy the CloudFormation stack described by `pipeline.yml` to
CloudFormation. This can be done from AWS Console, or from AWS CLI with the
following command:

```sh
export AWS_DEFAULT_REGION="your-region"
export AWS_PROFILE="your-profile-name"
aws cloudformation deploy \
    --template-file pipeline.yml \
    --capabilities CAPABILITY_NAMED_IAM \
    --stack-name hello-pipeline-cd-stack
```
